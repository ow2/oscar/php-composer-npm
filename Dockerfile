FROM composer:latest as COMPOSER

RUN \
	--mount=type=cache,target=/var/cache/apk \
	apk add \
	npm \
	gettext

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && \
	install-php-extensions gd intl mysqli xdebug


ENTRYPOINT ["/bin/sh", "-c"]
